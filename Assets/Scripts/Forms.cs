using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Forms
{
    public static int[,] Blinker( int[,] _map, int startX, int startY )
    {
        _map[startX, startY]   = 1;
        _map[startX+1, startY] = 1;
        _map[startX+2, startY] = 1;
        return _map;
    }

    public static int[,] Block( int[,] _map, int startX, int startY )
    {
        _map[startX, startY] = 1;
        _map[startX + 1, startY] = 1;
        _map[startX, startY + 1] = 1;
        _map[startX+1, startY + 1] = 1;
        return _map;
    }
}
