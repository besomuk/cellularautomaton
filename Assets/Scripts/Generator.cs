/*
Game oF life rules
    Any live cell with two or three live neighbours survives.
    Any dead cell with three live neighbours becomes a live cell.
    All other live cells die in the next generation. Similarly, all other dead cells stay dead.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public static Generator instance;

    [Header( "Map parameters" )]
    public int width;  // map width
    public int height; // map height
    public bool useRandomSeed;
    public string seed;
    [Range(0,100)]  
    public int fillPercent = 50;
    public bool autoStart = false;

    [Header("Game objects")]
    public GameObject aliveCell; // alive cell tile
    public GameObject blankTile; // blank tile. It's not 'dead' because it's empty.

    [Header( "Automata" )]
    public bool autoGenerationAdvance = false; // automatic generation advace or mouse click advance?
    public float timeBetweenGens      = 1.0f;  // time between 2 generations ( seconds )
    public int numberOfGenerations    = 0;     // total number of generations ( 0 - infinite )

    int totalCells = 0; // total cells in map

    int[,] map;          // this 2d array is map of our game. 
    int[,] nextGenMap;   // this one is map of next generation.
    int[,] originalMap;  // i'll need this when restarting current setup. map should be copied into this one on restart.

    private float t = 0; // timer
    private UIManager ui;

    private void Awake()
    {
        if ( !instance )
            instance = this;

        ui = GetComponent<UIManager>();
    }

    private void Start()
    {
        if ( autoStart )
            GameData.gamePlaying = true;
        GenerateMap();

        GameData.gamePlaying = false;
        // fill
        map = Tools.FillMap( map, 0 );
        //map = Tools.RandomFillMap( map, fillPercent, useRandomSeed, seed );

        //Tools.DebugShowMap(map);

        DrawGrid();

        // set data for display
        GameData.timeBetweenGenerations = timeBetweenGens;

        ui.UpdateIU();
    }

    // initialize map
    void GenerateMap()
    {
        map = new int[width, height];
        nextGenMap = new int[width, height];
        totalCells = width * height;
    }

    void IterateMap()
    {
        //Debug.Log( "Generation: " + GameData.totalGenerations );
        for ( int x = 0; x < width; x++ )
        {
            for ( int y = 0; y < height; y++ )
            {
                int nC = GetNeighbourCells( x, y );

                // debug
                /*
                if ( map[x, y] == 1 )
                    Debug.Log( x + " , " + y + " : " + nC );
                */

                // apply rules
                // value is assigned to nextGenMap, we don't want to spoil our original map ( until all cells are checked )
                if ( map[x, y] == 1 )
                {
                    if ( nC < 2 )
                    {
                        nextGenMap[x, y] = 0;
                    }
                    else  if ( nC >= 2 && nC <= 3 )
                    {
                        nextGenMap[x, y] = 1;
                    }
                    else if ( nC > 2 )
                    {
                        nextGenMap[x, y] = 0;
                    }
                }
                else if ( map[x, y] == 0 )
                {
                    if ( nC == 3 )
                        nextGenMap[x, y] = 1;
                }
            }
        }
    }

    // returns number of neighbouring cells
    int GetNeighbourCells ( int _x, int _y)
    {
        int nearCells = 0;

        // make little grid of 3x3 arround input cells
        for ( int nX = _x - 1; nX <= _x + 1; nX++ )
        {
            for ( int nY = _y - 1; nY <= _y + 1; nY++ )
            {
                // check for 'walls'
                if ( nX >= 0 && nX < width && nY >= 0 && nY < height ) 
                {
                    // do only cells around me
                    if ( nX != _x || nY != _y )
                    {
                        if ( map[nX,nY] == 1 )
                            nearCells++;
                    }
                }
                else
                {
                    // else what?
                }
            }
        }

        return nearCells;
    }

    // debug helper
    private void OnDrawGizmos()
    {
        if ( map != null )
        {
            for ( int x = 0; x < width; x++ )
            {
                for ( int y = 0; y < height; y++ )
                {
                    Gizmos.color = (map[x, y] == 1) ? Color.white : Color.black;
                    Vector3 pos = new Vector3( -width / 2 + x + 0.5f, -height / 2 + y + 0.5f, 0 );
                    //Vector3 pos = new Vector3( -width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f );
                    Gizmos.DrawCube( pos, Vector3.one );
                }
            }
        }
    }

    /// <summary>
    /// Draws what's inside map[,].
    /// </summary>
    void DrawGrid()
    {
        // calculate start positions so grid is displayed on the center of the screen
        float tempX = 0 - width  * blankTile.transform.localScale.x / 2;
        float tempY = 0 + height * blankTile.transform.localScale.y / 2;

        Vector3 startPosition = new Vector3( tempX, tempY, 0 ); ;
        Vector3 currentPosition = startPosition;
        // offset is equal tiles width, that is it's scale. All tiles are same size, so i'm using blank...
        Vector3 offset = new Vector3( blankTile.transform.localScale.x, blankTile.transform.localScale.y, 0 );

        // first destroy all present objects... not very good, but it works
        GameObject[] tiles = GameObject.FindGameObjectsWithTag( "cellTile" );
        if ( tiles.Length > 0 )
        {
            foreach ( GameObject g in tiles )
            {
                Destroy( g );
            }
        }

        if ( map != null )
        {
            for ( int y = height -1 ; y >= 0; y-- )
            {
                for ( int x = 0; x < width; x++ )
                {
                    GameObject chosenCell = map[x, y] == 0 ? blankTile : aliveCell; // if 0, show dead tile, if 1 show alive cell
                    GameObject go = Instantiate( chosenCell );
                    go.transform.position = currentPosition;
                    go.transform.parent = this.transform;
                    go.name = x + ":" + y;
                    go.GetComponent<SpriteInfo>().x = x;
                    go.GetComponent<SpriteInfo>().y = y;
                    currentPosition = new Vector3( currentPosition.x + offset.x, currentPosition.y, 0 );
                }
                currentPosition = new Vector3( startPosition.x, currentPosition.y - offset.y, 0 );
            }
        }
        ui.UpdateIU();
    }

    /// <summary>
    /// Creates and displays next generation. It's public because it's sometimes called outside of this class.
    /// </summary>
    public void NextGeneration()
    {
        if ( GameData.totalGenerations == 0 )
        {
            Debug.Log( "XXKXX - map copied to original map" );
            originalMap = map;
        }

        GameData.totalGenerations++;
        IterateMap();
        map = nextGenMap;                    // everything is done, assign nextGenMap to current map
        nextGenMap = new int[width, height]; // re-initialize nextGenMap, so it's ready for next iteration
        DrawGrid();
        ui.UpdateIU();
        //Tools.DebugShowMap(map);
    }

    /* Exposed methods ****************************************************************************** */
    /// <summary>
    /// Updates map array
    /// </summary>
    public void UpdateCell ( int _x, int _y, int _status )
    {
        map[_x, _y] = _status;
        DrawGrid();        
    }

    /// <summary>
    /// Exposed to generate random map
    /// </summary>
    public void GenerateRandomMap()
    {
        map = Tools.RandomFillMap( map, fillPercent, useRandomSeed, seed );
        DrawGrid();
    }

    public void RestartSetup()
    {
        Debug.Log( "XXKXX - Restarting current setup" );
        // to be implemented, but it goes something like this
        map = originalMap;
        nextGenMap = new int[width, height];
        GameData.totalGenerations = 0;
        DrawGrid();
    }

    public int GetCellValue( int x, int y)
    {
        return map[x, y];
    }

    public void ClearMap()
    {
        map = Tools.FillMap( map, 0 );
        nextGenMap = new int[width, height];
        GameData.totalGenerations = 0;
        DrawGrid();
    }

    /* Exposed methods end ************************************************************************** */

    private void Update()
    {
        if ( GameData.gamePlaying )
        {
            /*
            if ( !autoGenerationAdvance )
            {
                if ( Input.GetMouseButtonDown( 0 ) )
                {
                    NextGeneration();
                }
            }
            else
            */
            {
                Debug.LogFormat("between {0} ", timeBetweenGens);
                Debug.LogFormat("time {0} ", t);
                t += Time.deltaTime;
                if ( t > timeBetweenGens )
                {
                    if ( GameData.totalGenerations < numberOfGenerations || numberOfGenerations == 0 )
                    {
                        NextGeneration();
                        t = 0;
                    }
                }
            }
        }
    }

}
