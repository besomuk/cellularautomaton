using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteInfo : MonoBehaviour
{
    public int x;
    public int y;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUpAsButton()
    {
        int newCellValue = Generator.instance.GetCellValue( x, y ) == 0 ? 1 : 0; // new cell value is opposite of current value
        Generator.instance.UpdateCell( x, y, newCellValue );                     // update map cell
    }
}
