using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Text valGenerations;
    public Text tuneBtwGens;

    void Start()
    {
         
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKey( KeyCode.R ) )
        {
            RestartScene();
        }
    }

    private void RestartScene()
    {
        GameData.totalGenerations = 0;
        Scene s = SceneManager.GetActiveScene();
        SceneManager.LoadScene( s.name );
    }

    public void UpdateIU()
    {
        valGenerations.text = "Total generations: " + GameData.totalGenerations.ToString();
        tuneBtwGens.text = "Time between gens: " + GameData.timeBetweenGenerations.ToString() + "s";
    }

    public void ButtonStart()
    {
        Debug.Log( "Button start pressed" );
        GameData.gamePlaying = true;
    }

    public void ButtonStop()
    {
        Debug.Log( "Button stop pressed" );
        GameData.gamePlaying = false;
    }

    public void ButtonReset()
    {
        Generator.instance.RestartSetup();
    }

    public void ButtonAdvance()
    {
        Generator.instance.NextGeneration();
    }

    public void ButtonGenerateRandomMap()
    {
        Generator.instance.GenerateRandomMap();
    }

    public void ButtonClearMap()
    {
        Generator.instance.ClearMap();

    }
}
