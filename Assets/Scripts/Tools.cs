using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Some tools used all over the project
/// </summary>
public static class Tools
{
    /// <summary>
    /// Fills map with value.
    /// </summary>
    /// <param name="_map">map array</param>
    /// <param name="_value">array to be written in all fields</param>
    /// <returns>2D array with values</returns>
    public static int[,] FillMap( int[,] _map, int _value )
    {
        if ( _map != null )
        {
            Debug.Log( "Fill map with " + _value );
            for ( int x = 0; x < _map.GetLength( 0 ); x++ )
            {
                for ( int y = 0; y < _map.GetLength( 1 ); y++ )
                {
                    _map[x, y] = _value;
                }
            }
        }
        else
        {
            Debug.Log( "Map not initialized!" );
        }

        return _map;
    }

    /// <summary>
    /// Generates map with random values. Returns 2D array that represents map
    /// </summary>
    /// <param name="_map">map array</param>
    /// <param name="_fillPercent">How many cells will be filled in the begining? 
    /// If map has total of 100 cells, and fill percent is 50%, 50 cells will be filles with 1s</param>
    /// <returns></returns>
    public static int[,] RandomFillMap( int[,] _map, int _fillPercent, bool _useRandomSeed, string _seed )
    {
        if ( _map != null )
        {
            if ( _useRandomSeed )
            {
                _seed = System.DateTime.Now.ToString();
            }

            System.Random rnd = new System.Random( _seed.GetHashCode() );

            for ( int x = 0; x < _map.GetLength( 0 ); x++ )
            {
                for ( int y = 0; y < _map.GetLength( 1 ); y++ )
                {
                    int r = rnd.Next( 0, 100 );
                    if ( r < _fillPercent )
                        _map[x, y] = 1;
                    else
                        _map[x, y] = 0;
                }
            }
        }
        else
        {
            Debug.Log( "Map not initialized!" );
        }

        return _map;
    }

    /// <summary>
    /// Prints map in console using it's values
    /// </summary>
    /// <param name="_map"></param>
    public static void DebugShowMap( int[,] _map )
    {
        if ( _map != null )
        {
            for ( int x = _map.GetLength( 0 ) - 1; x >= 0; x-- )
            {
                string line = "";
                for ( int y = _map.GetLength( 1 ) - 1; y >= 0; y-- )
                {
                    line += " " + _map[y, x];
                }
                Debug.Log( line );
            }
        }
        else
        {
            Debug.Log( "Map not initialized!" );
        }
    }
}
