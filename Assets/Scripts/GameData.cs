using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public static int totalGenerations = 0;
    public static float timeBetweenGenerations = 0;
    public static bool gamePlaying = false;
}
